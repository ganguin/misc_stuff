#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv){
  if (argc != 3) {
    fprintf(stdout, "usage: %s </sys/class/object> <increment>\n", argv[0]);
    return 1;
  }

  int inc = atoi(argv[2]);
  char sys_class_max_brightness[128];
  int max_brightness = 0;
  char sys_class_brightness[128];
  int brightness = 0;

  snprintf(sys_class_max_brightness, 128,
           "/sys/class/%s/max_brightness", argv[1]);
  FILE* max_brightness_file = fopen(sys_class_max_brightness, "r");
  fscanf(max_brightness_file, "%d", &max_brightness);
  fclose(max_brightness_file);

  snprintf(sys_class_brightness, 128,
           "/sys/class/%s/brightness", argv[1]);
  FILE* brightness_file = fopen(sys_class_brightness, "r");
  fscanf(brightness_file, "%d", &brightness);
  fclose(brightness_file);

  int new_brightness = brightness + inc;
  if (new_brightness < 0) {
    new_brightness = 0;
  } else if (new_brightness > max_brightness) {
    new_brightness = max_brightness;
  }

  if (new_brightness != brightness) {
    brightness_file = fopen(sys_class_brightness, "w");
    fprintf(brightness_file, "%d", new_brightness);
    fclose(brightness_file);
  }

  return 0;
}
