#!/usr/bin/python3

import glob
import jeepney
import jeepney.integrate.blocking
import os
import psutil
import re
import stat


SMAPS="/proc/{}/smaps"
FD="/proc/{}/fd/{}"
ROOT="/proc/{}/root"

linked_files=set()

for process in psutil.process_iter():
    #TODO process with different root
    #root = os.readlink(ROOT.format(process.pid))
    root = ROOT.format(process.pid)
    #Process.memory_maps()
    with open(SMAPS.format(process.pid), "rb") as s:
        for line in s:
            fields = line.strip().split(None, 5)
            if len(fields) > 0 and \
               not fields[0].endswith(b":") and \
               len(fields) == 6:
                addr, perms, offset, dev, inode, path = fields
                inode = int(inode)
                path = path.decode()
                path = path.strip()
                if path.endswith(' (deleted)') and not os.path.exists(path):
                    path = path[:-10]
                linked_files.add((process.pid, inode, path, "smaps"))

    for openfile in process.open_files():
        fd_path = FD.format(process.pid, openfile.fd)
        inode = os.stat(fd_path).st_ino
        inode = int(inode)
        path = os.readlink(fd_path)
        if path.endswith(' (deleted)') and not os.path.exists(path):
            path = path[:-10]
        linked_files.add((process.pid, inode, path, f"fd: {openfile.fd}"))

sysbus = jeepney.integrate.blocking.connect_and_authenticate(bus='SYSTEM')
systemd1 = jeepney.DBusAddress('/org/freedesktop/systemd1',
                               bus_name='org.freedesktop.systemd1',
                               interface='org.freedesktop.systemd1.Manager')
## see /usr/share/dbus-1/system.d/org.freedesktop.systemd1.conf

sysd_escape = re.compile("_([0-9a-fA-F][0-9a-fA-F])")
def sysd_unescape(matchobj):
    code = int(matchobj.group(1), 16)
    return chr(code)

#systemctl daemon-reload
systemd_reload = jeepney.new_method_call(systemd1, 'Reload')
r = sysbus.send_and_get_reply(systemd_reload)

unit2pid_map = dict()
linked_files = sorted(linked_files)
for pid, inode, path, source in linked_files:
    isanomaly = False
    reason = "Unknown"
    if not os.path.exists(path) and inode != 0:
        #isanomaly = True
        #print("Anomaly: Process {} file inexistent in filesystem (inode {}): {}".format(pid, inode, path))
        if path.strip("0123456789.").endswith(".so"):
            isanomaly = True
            reason = f"File inexistent in filesystem, updated shared library? (inode {inode}, reason: {source}): {path}"
            #print("Anomaly: Process {} file inexistent in filesystem (inode {}): {}".format(pid, inode, path))
            alt_path = path.strip("0123456789.")[:-3].strip("0123456789.") + "*"
            alt_path_cand = glob.glob(alt_path)
            for cand in alt_path_cand:
                if cand.strip("0123456789.")[:-3].strip("0123456789.") == alt_path[:-1]:
                    #print("\tSimilar file in filesystem: {}".format(cand))
                    reason += ", candidate: " + cand

    elif os.path.exists(path) and os.stat(path).st_ino != inode and stat.S_ISREG(os.stat(path).st_mode):
        isanomaly = True
        reason = f"File mismatch filesystem, file has been replaced? (inode {inode}, reason {source}): {path}"
        #print("Anomaly: Process {} file mismatch filesystem (inode {}): {}".format(pid, inode, path))

    if isanomaly:
        systemd_GetUnitByPID = jeepney.new_method_call(systemd1, 'GetUnitByPID', 'u', (pid,))
        unit, = sysbus.send_and_get_reply(systemd_GetUnitByPID)
        if unit.startswith("/org/freedesktop/systemd1/unit/"):
            unit = unit[len("/org/freedesktop/systemd1/unit/"):]
            unit, _ = sysd_escape.subn(sysd_unescape, unit)
            unit2pid_map[unit] = unit2pid_map.get(unit, ()) + ((pid,reason),)
            #print("PID {} is part of systemd unit: {}".format(pid, unit))
        else:
            print(f"PID {pid} not found in systemd units")

for unit, anomalies in unit2pid_map.items():
    print(f"Unit {unit} needs to be restarted")
    for pid, reason in anomalies:
        print(f"\tPID {pid}, process {psutil.Process(pid).name()}:\n\t\t{reason}")

services = set()
systemd_services = set()
user_services = set()
scopes = set()
systemd_ListUnitsFiltered = jeepney.new_method_call(systemd1,
                                                    'ListUnitsFiltered',
                                                    'as',
                                                    (["running"],))
ListUnitsFiltered, = sysbus.send_and_get_reply(systemd_ListUnitsFiltered)
for unit in ListUnitsFiltered:
    unit = unit[0]
    if unit.endswith(".service"):
        if unit.startswith("user@"):
            user_services.add(unit)
        elif unit.startswith("systemd-"):
            systemd_services.add(unit)
        else:
            services.add(unit)
    elif unit.endswith("scope"):
        scopes.add(unit)

print("Standard services", services)
print("Systemd services", systemd_services)
print("User services", user_services)
print("Scopes", scopes)

print("#####################")
print("#####################")
print("")

to_restart = {u for u in unit2pid_map.keys()}
if "dbus.service" in unit2pid_map:
    print("# dbus needs to be restarted, systemd and systemd-services also needs to be restarted to re-register on dbus")
    print("systemctl restart dbus.service")
    print("systemctl daemon-reexec")
    print("systemctl restart " + " ".join(systemd_services))
    to_restart -= systemd_services | {"init.scope", "dbus.service"}
elif "init.scope" in unit2pid_map: # PID 1
    print("# systemd needs to be re-executed")
    print("systemctl daemon-reexec")
    to_restart -= {"init.scope"}

sub_restart = sorted({u for u in to_restart if u in services|systemd_services})
if sub_restart:
    print("# services to restart")
    print("systemctl restart " + " ".join(sub_restart))

sub_restart = sorted({u for u in to_restart if u in scopes})
if sub_restart:
    print("# scopes to restart")
    print("systemctl restart " + " ".join(sub_restart))

sub_restart = sorted({u for u in to_restart if u in user_services})
if sub_restart:
    print("# user services to restart")
    print("systemctl restart " + " ".join(sub_restart))

# job=manager.RestartUnit('sshd.service', 'fail')


