#!/bin/bash

function fail {
  echo $*
  exit 1
}

which pypi-download > /dev/null && which py2dsc-deb > /dev/null || fail "missing pypi-download/py2dsc-deb, please install them: apt install python3-stdeb"

# py2dsc-deb build option
export DEB_BUILD_OPTIONS=nocheck

# build frida egg and servers
rm -rf android-ndk-r20b-linux-x86_64.zip android-ndk-r20b frida frida-bin frida-python deb_dist

wget https://dl.google.com/android/repository/android-ndk-r20b-linux-x86_64.zip
unzip android-ndk-r20b-linux-x86_64.zip
export ANDROID_NDK_ROOT="$PWD/android-ndk-r20b"

export PY_VERSION="$(python3 -c 'import sys; print("{}.{}".format(sys.version_info.major, sys.version_info.minor))')"

git clone --recurse-submodules https://github.com/frida/frida.git
cd frida
export FRIDA_VERSION="$(git tag -l | sort -V | tail -n 1)"
export FRIDA_BASEDIR="$PWD"
make python-linux-x86_64 PYTHON=/usr/bin/python${PY_VERSION}

make core-linux-x86
make core-android-arm
make core-android-arm64

cd ..

mkdir frida-bin
cp -iv frida/build/frida-linux-x86/bin/frida-server frida-bin/frida-server-linux-x86-${FRIDA_VERSION}
cp -iv frida/build/frida-linux-x86_64/bin/frida-server frida-bin/frida-server-linux-x86_64-${FRIDA_VERSION}
cp -iv frida/build/frida-android-arm/bin/frida-server frida-bin/frida-server-android-arm-${FRIDA_VERSION}
cp -iv frida/build/frida-android-arm64/bin/frida-server frida-bin/frida-server-android-arm64-${FRIDA_VERSION}

git clone https://github.com/frida/frida-python
cd frida-python/
export FRIDA_EXTENSION="${FRIDA_BASEDIR}/build/frida-linux-x86_64/lib/python${PY_VERSION}/site-packages/_frida.so"
python${PY_VERSION} setup.py bdist_egg

cd ..

cp -iv frida-python/dist/frida-${FRIDA_VERSION}-py${PY_VERSION}-linux-x86_64.egg frida-bin

pypi-download frida --release=${FRIDA_VERSION}

# Shitty pypi package expects the egg to be found at a specific location (deb_dist/frida-${FRIDA_VERSION}/.pybuild/cpython3_${PY_VERSION}_frida/)
while true; do
    sleep 0.5;
    rsync -a frida-bin/frida-${FRIDA_VERSION}-py${PY_VERSION}-linux-x86_64.egg deb_dist/frida-${FRIDA_VERSION}/.pybuild/cpython3_${PY_VERSION}_frida/frida-${FRIDA_VERSION}-py${PY_VERSION}-linux-x86_64.egg;
done &

# this will build frida-python for the python version where /usr/bin/python3 points to
# therefore if multiple versions are installed, frida-python will be built for only one
# TODO to make it work for all installed versions, this script needs to modified to build the requirements for all python versions:
# * make python-linux-x86_64 PYTHON=/usr/bin/python${PY_VERSION}
# * python${PY_VERSION} setup.py bdist_egg
# * rsync in the while loop
export PYBUILD_INTERPRETERS="python${PY_VERSION}"
export PYBUILD_VERSIONS="${PY_VERSION}"
py2dsc-deb --with-python3=True --with-python2=False frida-${FRIDA_VERSION}.tar.gz
kill %1
cp -v deb_dist/*.deb .

rm -r deb_dist

