#!/bin/bash

function fail {
  echo $*
  exit 1
}

which pypi-download > /dev/null && which py2dsc-deb > /dev/null || fail "missing pypi-download/py2dsc-deb, please install them: apt install python3-stdeb"

package="$1"

# py2dsc-deb build options
export DEB_BUILD_OPTIONS=nocheck

echo "Build DEB package for $package"
PKG_VERSION="$(pypi-download $package)"
PKG_VERSION="${PKG_VERSION/OK: }"
# TODO sanity check that $PKG_VERSION is valid
py2dsc-deb --with-python3=True --with-python2=False "$PKG_VERSION" || fail "py2dsc-deb failed"
cp -v deb_dist/*.deb . || fail "deb packages not created"
rm -r deb_dist "$PKG_VERSION"
