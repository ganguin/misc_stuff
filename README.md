# Misc scripts repo

## brightness.c

Simple program to increase/decrease screen brightness via sysfs.

## certbot-gandi-dns01-hook.py

Certbot hook for Gandi LiveDNS (using --manual-auth-hook and --manual-cleanup-hook). Example usage:

```
mkdir config logs work
GANDI_API_KEY="xxx" certbot certonly --config-dir config/ --work-dir work/ --logs-dir logs/ --agree-tos --register-unsafely-without-email --manual-public-ip-logging-ok --manual --preferred-challenges=dns --manual-auth-hook ./certbot-gandi-dns01-hook.py --manual-cleanup-hook ./certbot-gandi-dns01-hook.py -d example.com -d *.example.com --test-cert
```

## frida2deb.sh

Build DEB packages for frida and frida-python.

Why? because pypi does not package EGGs for python > 3.6.

## egencache-git-changed

Gentoo portage postsync (git based) hook for updating cache for local changes.

## lsof-deleted.py

Check for running processes using deleted files (upgraded libraries?). Link them with their corresponding systemd services. It does not perform any actions, just prints the restart commands on stdout (and a lot of debug).

## pip2deb.sh

Build a DEB package from a pypi package (online fetch). Usage:

```
./pip2deb.sh <pypi-package-name>
```

## swaystatus

Probably the biggest project in here, an i3status-like for sway using asyncio (matches my use case and system, for now everyting is hardcoded):

* Focused window title - using swayipc events
* Disk usage - polling
* Sysdata CPU and RAM usage - polling
* Iwd WLAN monitor and on/off switch (mouse click) - using IWD dbus events
* Battery level - using udev monitor events and (adaptative) polling
* Alsa volume status and control (mouse scroll events) - using alsaaudio events
* Date and time

## swap-usage.py

Lists top 10 processes using the top most swap. Primarily designed to detect service with memory leaks in order to kill/restart them.
