#!/usr/bin/python3

import psutil

limit = 10
procs_swapping = []

for p in psutil.process_iter():
  p_dict = p.as_dict(attrs=['pid', 'name', 'username', 'memory_full_info'])
  p_dict['swap'] = p_dict['memory_full_info'].swap
  if p_dict['swap'] > 0:
    procs_swapping.append(p_dict)

print("PID\tUSER\tSWAP\tNAME")
sorted_procs = sorted(procs_swapping, reverse=True, key=lambda e: e['swap'])
for n, p in enumerate(sorted_procs):
  if n >= limit:
    break
  swap_human = p['swap'] // (1024**2)
  print(f"{p['pid']}\t{p['username']}\t{swap_human} MB\t{p['name']}")
