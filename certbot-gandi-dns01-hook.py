#!/usr/bin/python

import json
import os
import requests

domain = os.environ['CERTBOT_DOMAIN']
validation = os.environ['CERTBOT_VALIDATION']
auth_output = os.environ.get('CERTBOT_AUTH_OUTPUT')

apikey = os.environ['GANDI_API_KEY']
apiurl = f"https://dns.api.gandi.net/api/v5/domains/{domain}/records/_acme-challenge/TXT"

headers = {"X-Api-Key": apikey}

if auth_output is None: # auth

  r = requests.get(apiurl, headers=headers)

  if r.status_code == 200: # _acme-challenge TXT exists, append (PUT)
    txt_values = r.json()['rrset_values']
    requests_method = requests.put
  else: # no _acme-challenge TXT, create new one (POST)
    txt_values = []
    requests_method = requests.post
  txt_values.append(validation)

  headers["Content-Type"] = "application/json"

  data = { "rrset_ttl": 300, # short lifetime
           "rrset_values": txt_values}

  r = requests_method(apiurl, data=json.dumps(data), headers=headers)

else: # cleanup

  r = requests.delete(apiurl, headers=headers)
